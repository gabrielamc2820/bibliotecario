package com.ceiba.biblioteca.infraestructura.controllador;

import com.ceiba.biblioteca.aplicacion.manejadores.prestamo.ManejadorGenerarPrestamo;
import com.ceiba.biblioteca.aplicacion.manejadores.prestamo.ManejadorObtenerPrestamo;
import com.ceiba.biblioteca.dominio.Prestamo;
import com.ceiba.biblioteca.dominio.servicio.bibliotecario.ServicioBibliotecario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/prestamos")
public class ControladorPrestamo {
    private final ManejadorObtenerPrestamo manejadorObtenerPrestamo;
    @Autowired
    private ServicioBibliotecario servicioBibliotecario;

    public ControladorPrestamo(ManejadorObtenerPrestamo manejadorObtenerPrestamo) {
        this.manejadorObtenerPrestamo = manejadorObtenerPrestamo;
    }

    @PostMapping("/{isbn}/{nombreCliente}")
    public void prestar(@PathVariable(name = "isbn") String isbn, @PathVariable(name = "nombreUsuario") String nombreUsuario) {
    	try {
			this.servicioBibliotecario.prestar(isbn, nombreUsuario);
		} catch (Exception e) {
			throw new UnsupportedOperationException(e);
		}
        
    }

    @GetMapping("/{isbn}")
    public Prestamo obtenerLibroPrestadoPorIsbn(@PathVariable(name = "isbn") String isbn) {
        return this.manejadorObtenerPrestamo.ejecutar(isbn);
    }
}
