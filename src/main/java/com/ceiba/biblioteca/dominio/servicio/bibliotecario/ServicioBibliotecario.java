package com.ceiba.biblioteca.dominio.servicio.bibliotecario;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.ceiba.biblioteca.dominio.Libro;
import com.ceiba.biblioteca.dominio.Prestamo;
import com.ceiba.biblioteca.dominio.repositorio.RepositorioLibro;
import com.ceiba.biblioteca.dominio.repositorio.RepositorioPrestamo;

@Service
public class ServicioBibliotecario {

	public static final String EL_LIBRO_NO_SE_ENCUENTRA_DISPONIBLE = "El libro no se encuentra disponible";
	public static final int SUMA_MAYOR = 30;
	public static final int DIAS_MAXIMOS_ENTREGA = 15;
	public static final String LIBRO_POLINDROMO = "los libros palindromos solo se pueden utilizar en la biblioteca";

	private final RepositorioLibro repositorioLibro;
	private final RepositorioPrestamo repositorioPrestamo;

	public ServicioBibliotecario(RepositorioLibro repositorioLibro, RepositorioPrestamo repositorioPrestamo) {
		this.repositorioLibro = repositorioLibro;
		this.repositorioPrestamo = repositorioPrestamo;
	}

	public void prestar(String isbn, String nombreUsuario){
		//PUNTO 3 
		if(!this.esPalindromo(isbn)) {
			// PUNTO 1
			Libro libro = this.repositorioLibro.obtenerPorIsbn(isbn);
			if(libro != null) {
				// PUNTO 2
				if(!this.esPrestado(isbn)) {

					// PUNTO 4
					Prestamo nuevoPrestamo = new Prestamo(libro);
					nuevoPrestamo.setNombreUsuario(nombreUsuario);
					//PUNTO 5
					char[] digitosIsbn = isbn.toCharArray();
					int sumaTotal = 0;
					for (char c : digitosIsbn) {
						if(isNumeric(String.valueOf(c))) {
							sumaTotal = sumaTotal+c;
						}
					}
					if(sumaTotal > SUMA_MAYOR) {
						nuevoPrestamo.setFechaEntregaMaxima(fechaEntregaMaxima());

					}
					else {
						nuevoPrestamo.setFechaEntregaMaxima(null);
					}
					this.repositorioPrestamo.agregar(nuevoPrestamo);

				}
				else {
					throw new UnsupportedOperationException(EL_LIBRO_NO_SE_ENCUENTRA_DISPONIBLE);
				}
			}
		}
		else {
			throw new UnsupportedOperationException(LIBRO_POLINDROMO);
		}
	}

	private static boolean isNumeric(String isbn){
		try {
			Integer.parseInt(isbn);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}

	public Date fechaEntregaMaxima(){
		Date fechaActual = new Date();
		Calendar fechaFin = Calendar.getInstance();
		fechaFin.setTime(fechaActual);
		fechaFin.add(Calendar.DAY_OF_YEAR, DIAS_MAXIMOS_ENTREGA);
		int fin = fechaFin.DAY_OF_WEEK;
		if(fin != Calendar.SUNDAY) {
			fechaFin.add(Calendar.DAY_OF_YEAR, 1);
		}
		return fechaFin.getTime();

	}

	public boolean esPalindromo(String isbn) {
		isbn = isbn.toLowerCase().replace("á", "a").replace("é", "e").replace("í", "i").replace("ó", "o")
				.replace("ú", "u").replace(" ", "").replace(".", "").replace(",", "");
		String invertida = new StringBuilder(isbn).reverse().toString();
		return invertida.equals(isbn);
	}

	public boolean esPrestado(String isbn) {
		Libro libro = this.repositorioPrestamo.obtenerLibroPrestadoPorIsbn(isbn);
		return libro == null ? false:true;
	}
}
